//
// assets
// ----------------------------------------------------------------------------

'use strict';

const gulp         = require('gulp');
const config       = require('../config.json');


// copy:assets
// Copie les assets dans public
// ----------------------------------------------------------------------------

gulp.task('copy:assets', () => {
  return gulp.src([
    config.paths.assets + 'project/**',
    '!' + config.paths.assets + 'project/{scss,scss/**}',
    '!' + config.paths.assets + 'project/{scripts,scripts/**}'])
    .pipe(gulp.dest(config.paths.build + 'assets'));
});


// copy:favicon
// Ajoute la favicon à la racine de public

gulp.task('copy:favicon', () => {
  return gulp.src(config.paths.assets + 'project/favicon/favicon.ico')
    .pipe(gulp.dest(config.paths.build));
});
