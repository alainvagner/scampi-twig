//
// CSS
// ----------------------------------------------------------------------------

'use strict';

const gulp         = require('gulp');
const config       = require('../config.json');
const sass         = require('gulp-sass');
const autoprefixer = require('gulp-autoprefixer');
const sourcemaps   = require('gulp-sourcemaps');
const minCss       = require('gulp-clean-css');
const rename       = require("gulp-rename");


// make:css
// Compile les css
// ----------------------------------------------------------------------------

gulp.task('make:css',  () => {
  return gulp.src(config.paths.assets + 'project/scss/**/*.scss')
  .pipe(sourcemaps.init())
    .pipe(sass({
      outputStyle: 'expanded',
      includePaths: ['node_modules']
    }).on('error', sass.logError))
    .pipe(autoprefixer('last 2 version'))
  .pipe(sourcemaps.write('maps'))
  .pipe(gulp.dest(config.paths.build + 'assets/css'))
  .pipe(minCss())
  .pipe(rename({ extname: '.min.css' }))
  .pipe(gulp.dest(config.paths.build + 'assets/css'))
  callback()
});
