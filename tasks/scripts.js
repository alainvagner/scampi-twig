//
// Scripts
// ----------------------------------------------------------------------------

'use strict';

const gulp         = require('gulp');
const config       = require('../config.json');
const parcel       = require('gulp-parcel');


// copy:js-vendors
// Envoie les dépendences js dans public
// ----------------------------------------------------------------------------
// gulp.task('copy:js-vendors', () => {
//   return gulp.src(config.paths.assets + 'scampi/scripts/has-js.js')
//     .pipe(gulp.dest(config.paths.build + 'assets/scripts/vendors'));
// });

// make:js
// Concaténation des scripts du projet (scripts/main/)
// ----------------------------------------------------------------------------

gulp.task('make:js-main', () => {
  return gulp.src([config.paths.assets +'project/scripts/main/main.js'], {read:false})
  .pipe(parcel())
  .pipe(gulp.dest(config.paths.build + 'assets/scripts'))
});
