//
// Build
// ----------------------------------------------------------------------------

'use strict';

const gulp         = require('gulp');
const config       = require('../config.json');
const runSequence  = require('gulp4-run-sequence');
const argv         = require('yargs').argv;


// variables liées à l'environnement
if (argv.prod) {
  var baseURL = config.baseURL.prod,
      env = "prod";
}
else {
  var baseURL = config.baseURL.dev;
}


// build
// Construit le répertoire public
// ----------------------------------------------------------------------------
gulp.task('build', function (callback) {
  runSequence(
    'clean',
    ['make:html',
    'copy:assets','copy:favicon',
    // 'copy:js-vendors',
    'make:js-main','make:css'],
    'make:html-prettify',
    callback);
  },
  {
    options: {
      'dev': '[build par défaut] baseURL = config.baseURL.dev',
      'prod': 'baseURL = config.baseURL.dev',
    }
  }
);


// dev
// Enchaîne les tâches build + live
// ----------------------------------------------------------------------------
gulp.task('dev', function (callback) {
  runSequence(
    'build',
    'live',
    callback);
  }
);
