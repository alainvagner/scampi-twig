changelog
====================================================================

Version 0.6
-----------

### Mai 2020
- passage à gulp4
- référence à la version la plus récente de scampi
- inclusion du script anchor-focus.js comme un script à importer par défaut dans tous les projets (gestion du focus suivant un focus ancré).

Version 0.5
-----------

### Décembre 2018

#### Infra
- mise à jour du package npm

#### Fragments
- mise à jour des fragments pour refléter les changements intervenus sur la dernière version de Scampi, notamment svg-icons, links, forms, buttons

#### Templates
- le bloc des liens d'accès rapide a été supprimé de la zone banner et placé en premier dans la source
- en production, on importe le fichier minifié des css



Version 0.4
-----------

### octobre 2018

#### Infra
- mise à jour du package npm
- renommage _assets > assets
- mise à jour de Scampi (à compléter, branche develop pour l'instant)
- normalisation du nommage des tâches Gulp
- le build permet de choisir une url de base optionnelle définie dans config.json
- la tâche par défaut "gulp" liste les tâches disponibles

#### Index
- création d'un template et d'une css spécifiques pour l'index de projet

#### Styleguide
- normalisation et actualisation des fragments pour le styleguide
- ajout d'un lien de retour vers l'index du projet depuis le styleguide
- ajout d'une class (.sg) sur le body du styleguide pour pouvoir cibler des styles spécifiques

#### Templates
- intégration par défaut dans les gabarits (et les css) des éléments correspondant aux obligations/recommandations de la checklist Pidila : liens d'évitement, sprite svg, breadcrumb, browsehappy
- macros twig d'usage courant (liens, icônes) > dev/templates/common/macro-generic.twig
- import dans le template de base de la feuille de style en fonction de l'environnement : en dev, la css simplement compilée ; en prod, la css compilée et minifiée
