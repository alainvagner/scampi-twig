import { scampi, collapse, skipLinks, svgXuse, uPalette, anchorFocus } from '@pidila/scampi'

document.addEventListener('DOMContentLoaded', function() {
    scampi.init()
    collapse()
    skipLinks()
    svgXuse()
    uPalette()
    anchorFocus()
})